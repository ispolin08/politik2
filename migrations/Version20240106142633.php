<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240106142633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE TABLE messenger_messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, body CLOB NOT NULL, headers CLOB NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , available_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , delivered_at DATETIME DEFAULT NULL --(DC2Type:datetime_immutable)
        )'
        );
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql(
            'CREATE TEMPORARY TABLE __temp__member AS SELECT id, full_name, external_id, country, political_group, national_political_group, contacts FROM member'
        );
        $this->addSql('DROP TABLE member');
        $this->addSql(
            'CREATE TABLE member (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, external_id INTEGER NOT NULL, country VARCHAR(255) NOT NULL, political_group VARCHAR(255) NOT NULL, national_political_group VARCHAR(255) NOT NULL, contacts CLOB DEFAULT NULL --(DC2Type:json)
        )'
        );
        $this->addSql(
            'INSERT INTO member (id, full_name, external_id, country, political_group, national_political_group, contacts) SELECT id, full_name, external_id, country, political_group, national_political_group, contacts FROM __temp__member'
        );
        $this->addSql('DROP TABLE __temp__member');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql(
            'CREATE TEMPORARY TABLE __temp__member AS SELECT id, full_name, external_id, country, political_group, national_political_group, contacts FROM member'
        );
        $this->addSql('DROP TABLE member');
        $this->addSql(
            'CREATE TABLE member (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, external_id INTEGER NOT NULL, country VARCHAR(255) NOT NULL, political_group VARCHAR(255) NOT NULL, national_political_group VARCHAR(255) NOT NULL, contacts CLOB DEFAULT NULL)'
        );
        $this->addSql(
            'INSERT INTO member (id, full_name, external_id, country, political_group, national_political_group, contacts) SELECT id, full_name, external_id, country, political_group, national_political_group, contacts FROM __temp__member'
        );
        $this->addSql('DROP TABLE __temp__member');
    }
}
