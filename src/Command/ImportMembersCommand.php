<?php

namespace App\Command;

use App\Service\MembersImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:import-members')]
final class ImportMembersCommand extends Command
{

    private MembersImporterInterface $membersImporter;

    public function __construct(MembersImporterInterface $membersImporter)
    {
        $this->membersImporter = $membersImporter;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->membersImporter->import();
        $output->writeln('DONE');

        return Command::SUCCESS;
    }
}