<?php

namespace App\Service;

use App\DTO\MemberContactDTO;
use App\DTO\MemberDTO;
use App\Message\Member;
use App\Repository\MemberRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MembersImporter implements MembersImporterInterface
{
    private const SOURCE_URL = 'https://www.europarl.europa.eu/meps/en/full-list/xml/a';
    private const DETAILS_URL_TEMPLATE = 'https://www.europarl.europa.eu/meps/en/%d/MAGDALENA_ADAMOWICZ/home';

    public function __construct(
        readonly private HttpClientInterface $client,
        readonly private MemberRepository $memberRepository,
        readonly private MessageBusInterface $bus
    )
    {
    }

    public function import()
    {
        $membersList = $this->getMembersList();

        if (!count($membersList)) {
            return;
        }

        foreach ($membersList as $member) {
            $this->bus->dispatch(new Member($member));
        }

    }

    public function fillContacts(MemberDTO $memberDTO)
    {
        $response = $this->client->request(
            'GET',
            sprintf(self::DETAILS_URL_TEMPLATE, $memberDTO->getId())
        );

        $crawler = new Crawler($response->getContent());

        $contactCards = $crawler
            ->filter('div.erpl_contact-card-list > span');

        foreach ($contactCards as $card) {
            $memberDTO->addContact(new MemberContactDTO('Address', $card->textContent));
        }

        $socials = $crawler
            ->filter('div.erpl_social-share-horizontal > a')
            ->extract(['href', 'data-original-title']);

        foreach ($socials as $social) {
            if ($social[1] === 'E-mail') {
                $social[0] = $this->fixEmail($social[0]);
            }
            $memberDTO->addContact(new MemberContactDTO($social[1], $social[0]));
        }
    }

    public function store(MemberDTO $member)
    {
        $this->memberRepository->upsert($member);
    }

    private function fixEmail(string $email)
    {
        $from = ['[at]', '[dot]'];
        $to = ['@', '.'];

        return str_replace($from, $to, $email);
    }

    // Could be shifted in new class and DInjected in this importer
    private function getMembersList(): ArrayCollection
    {
        $response = $this->client->request(
            'GET',
            self::SOURCE_URL
        );

        $result = new ArrayCollection();

        $encoders = [new XmlEncoder()];
        $serializer = new Serializer([], $encoders);
        $members = $serializer->decode($response->getContent(), 'xml');

        foreach ($members['mep'] as $member) {
            $result->add(
                new MemberDTO(
                    $member['fullName'],
                    $member['country'],
                    $member['politicalGroup'],
                    $member['id'],
                    $member['nationalPoliticalGroup']
                )
            );
        }

        return $result;

    }

}