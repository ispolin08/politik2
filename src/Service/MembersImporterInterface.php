<?php

namespace App\Service;

interface MembersImporterInterface
{
    public function import();
}