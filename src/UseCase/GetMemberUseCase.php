<?php

namespace App\UseCase;

use App\Repository\MemberRepository;
use App\RestResource\MemberFullRestResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetMemberUseCase
{

    private MemberRepository $memberRepository;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function execute(int $memberId): Response
    {
        $member = $this->memberRepository->find($memberId);

        if (!$member) {
            throw new NotFoundHttpException('Member not found');
        }

        return new JsonResponse(
            MemberFullRestResource::createFromEntity($member)
        );
    }

}