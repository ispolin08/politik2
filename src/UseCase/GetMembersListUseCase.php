<?php

namespace App\UseCase;

use App\Repository\MemberRepository;
use App\RestResource\CollectionRestResource;
use App\RestResource\MemberRestResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetMembersListUseCase
{

    private MemberRepository $memberRepository;

    /**
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function execute(): Response
    {
        // TODO Support pagination
        $members = $this->memberRepository->findBy([], null, 100);

        $resourcesList = array();
        foreach ($members as $member) {
            $resourcesList[] = MemberRestResource::createFromEntity($member);
        }

        return new JsonResponse(
            new CollectionRestResource($resourcesList, 1)
        );
    }

}