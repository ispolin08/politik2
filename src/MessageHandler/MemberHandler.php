<?php

namespace App\MessageHandler;

use App\Message\Member;
use App\Service\MembersImporter;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class MemberHandler
{

    public function __construct(
        readonly private MembersImporter $membersImporter
    ) {
    }

    public function __invoke(Member $message)
    {
        $this->membersImporter->fillContacts($message->getMemberDTO());
        $this->membersImporter->store($message->getMemberDTO());
    }
}