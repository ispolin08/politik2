<?php

namespace App\Controller;

use App\UseCase\GetMembersListUseCase;
use App\UseCase\GetMemberUseCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class MemberController
{
    #[Route('/members')]
    public function members(GetMembersListUseCase $useCase): Response
    {
        return $useCase->execute();
    }

    #[Route('/members/{id}')]
    public function member(int $id, GetMemberUseCase $useCase): Response
    {
        return $useCase->execute($id);
    }

}