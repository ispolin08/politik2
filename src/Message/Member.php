<?php

namespace App\Message;

use App\DTO\MemberDTO;

class Member
{
    public function __construct(
        readonly private MemberDTO $memberDTO,
    ) {
    }

    public function getMemberDTO(): MemberDTO
    {
        return $this->memberDTO;
    }
}