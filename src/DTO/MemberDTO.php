<?php

namespace App\DTO;

class MemberDTO
{
    private string $fullName;
    private string $country;
    private string $politicalGroup;
    private int $id;
    private string $nationalPoliticalGroup;
    private array $contacts;

    public function __construct(
        string $fullName,
        string $country,
        string $politicalGroup,
        int $id,
        string $nationalPoliticalGroup
    ) {
        $this->fullName = $fullName;
        $this->country = $country;
        $this->politicalGroup = $politicalGroup;
        $this->id = $id;
        $this->nationalPoliticalGroup = $nationalPoliticalGroup;
        $this->contacts = [];
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getPoliticalGroup(): string
    {
        return $this->politicalGroup;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNationalPoliticalGroup(): string
    {
        return $this->nationalPoliticalGroup;
    }

    public function addContact(MemberContactDTO $contact)
    {
        $this->contacts[] = $contact;
    }

    public function addSocial(string $social)
    {
        $this->socials[] = $social;
    }

    /**
     * @return MemberContactDTO[]
     */
    public function getContacts(): array
    {
        return $this->contacts;
    }

}