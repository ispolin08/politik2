<?php

namespace App\Repository;


use App\DTO\MemberDTO;
use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    public function upsert(MemberDTO $memberDTO)
    {

        $existing = $this->findOneBy(['externalId' => $memberDTO->getId()]);

        if (!$existing) {
            $existing = new Member();
            $existing->setExternalId($memberDTO->getId());
            $this->_em->persist($existing);
        }

        $existing->setFullName($memberDTO->getFullName());
        $existing->setCountry($memberDTO->getCountry());
        $existing->setNationalPoliticalGroup($memberDTO->getNationalPoliticalGroup());
        $existing->setPoliticalGroup($memberDTO->getPoliticalGroup());

        $entityContacts = [];
        foreach ($memberDTO->getContacts() as $contact) {
            $entityContacts[] = [
                'type' => $contact->getType(),
                'value' => $contact->getValue(),
            ];
        }

        $existing->setContacts($entityContacts);

        $this->_em->flush();
    }

}