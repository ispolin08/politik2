<?php

namespace App\RestResource;

class MemberContactRestResource
{
    public string $type;
    public string $value;

    static function createFromArray(array $data): self
    {
        $result = new self();
        $result->type = self::defineType($data['type']);
        $result->value = $data['value'];

        return $result;
    }

    private static function defineType($type): string
    {
        switch ($type) {
            case 'Address':
                return 'address';
            case 'E-mail':
                return 'email';
            default:
                return 'social';
        }
    }
}