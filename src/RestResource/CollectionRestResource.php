<?php

namespace App\RestResource;

class CollectionRestResource
{
    public array $data;
    public int $page;

    public function __construct(array $data, int $page)
    {
        $this->data = $data;
        $this->page = $page;
    }

}