<?php

namespace App\RestResource;

use App\Entity\Member;

class MemberFullRestResource extends MemberRestResource
{
    public array $contacts;

    public static function createFromEntity(Member $member): self
    {
        // TODO fix code duplicity
        $result = new self();
        $names = explode(' ', $member->getFullName());
        $result->firstName = $names[0];
        $result->lastName = $names[1];
        $result->county = $member->getCountry();
        $result->politicalGroup = $member->getPoliticalGroup();
        $result->id = $member->getId();

        foreach ($member->getContacts() as $contact) {
            $result->contacts[] = MemberContactRestResource::createFromArray($contact);
        }

        return $result;
    }

}