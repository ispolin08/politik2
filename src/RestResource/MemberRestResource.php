<?php

namespace App\RestResource;

use App\Entity\Member;

class MemberRestResource
{
    public int $id;
    public string $firstName;
    public string $lastName;
    public string $county;
    public string $politicalGroup;

    public static function createFromEntity(Member $member): self
    {
        $result = new self();
        $names = explode(' ', $member->getFullName());
        $result->firstName = $names[0];
        $result->lastName = $names[1];
        $result->county = $member->getCountry();
        $result->politicalGroup = $member->getPoliticalGroup();
        $result->id = $member->getId();

        return $result;
    }
}