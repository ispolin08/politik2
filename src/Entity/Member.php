<?php

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MemberRepository::class)]
class Member
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $fullName = null;

    #[ORM\Column]
    private int $externalId;

    // TODO shift to another table and make relations
    #[ORM\Column]
    private string $country;

    // TODO shift to another table and make relations
    #[ORM\Column]
    private string $politicalGroup;

    // TODO shift to another table and make relations
    #[ORM\Column]
    private string $nationalPoliticalGroup;

    #[ORM\Column(type: "json", nullable: true)]
    private array $contacts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getExternalId(): int
    {
        return $this->externalId;
    }

    public function setExternalId(int $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    public function getPoliticalGroup(): string
    {
        return $this->politicalGroup;
    }

    public function setPoliticalGroup(string $politicalGroup): void
    {
        $this->politicalGroup = $politicalGroup;
    }

    public function getNationalPoliticalGroup(): string
    {
        return $this->nationalPoliticalGroup;
    }

    public function setNationalPoliticalGroup(string $nationalPoliticalGroup): void
    {
        $this->nationalPoliticalGroup = $nationalPoliticalGroup;
    }

    public function setContacts(array $contacts): void
    {
        $this->contacts = $contacts;
    }

    public function getContacts(): array
    {
        return $this->contacts;
    }
}
